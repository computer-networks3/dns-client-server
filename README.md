# DNS Client Server

Reference: https://datatracker.ietf.org/doc/html/rfc2616 

DNS Server: supporting all types of queries and replies.
Should be able to do both recursive and iterative queries.
Caching to be implemented.

Client like nslookup: as close as possible to the existing nslookup, all options, all functionality, use of the file /etc/resolv.conf

How to compile and run the code

Run the server in one terminal by typing the command 'python3 dns-server.py'. On the other terminal run client side commands one by one

Functions implemented

1. Types of queries: a, aaaa, cname, ns, mx, ptr, any
2. Inverse(reverse) queries
3. Set a server other than the default.
4. Set a port number other than the default.
5. Set timeout for the request
6. Set recursive or non-recursive queries
7. Find the values of all parameters(server, port, timeout, query type, class)
8. Caching of responses at the local dns server according to their time to live.
